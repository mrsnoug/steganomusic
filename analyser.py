import copy
from typing import Tuple

import numpy as np
from music21 import midi, note, chord, duration
from tqdm import tqdm

from predict import MusicGenerator


class MidiHelper:
    """Simple helper class to work with MIDI files. This one has the same methods as the one
    in stat_analysis/stat_analyser.py, however I could not have used that one since there
    would be a circle import, and we don't like those. And yes - it is not necessary to make
    this one a class - function would be sufficient."""

    def __init__(self, midi_file: str):
        mf = midi.MidiFile()
        mf.open(midi_file)
        mf.read()
        mf.close()
        self.base_midi_stream = midi.translate.midiFileToStream(mf)

    def list_instruments(self):
        part_stream = self.base_midi_stream.parts.stream()
        print("List of instruments found in MIDI file:")
        for p in part_stream:
            print(p.partName)

    def get_midi_notes_list(self):
        return [nt for nt in self.base_midi_stream.parts[0].flat.notes]

    def extract_notes(self, index: int):
        list_of_notes = []
        midi_value = []
        note_names = []
        for nt in self.base_midi_stream.parts[index].flat.notes:
            if isinstance(nt, note.Note):
                midi_value.append(max(0.0, nt.pitch.ps))
                list_of_notes.append(nt)
                note_names.append(nt.pitch.nameWithOctave)
            elif isinstance(nt, chord.Chord):
                chord_pitches = []
                for pitch in nt.pitches:
                    midi_value.append(max(0.0, pitch.ps))
                    list_of_notes.append(nt)
                    chord_pitches.append(pitch.nameWithOctave)
                note_names.append(chord_pitches)

        return midi_value, list_of_notes, note_names


class SteganoMusic:
    """Class for creating steganographic music files. For the complete algorithm breakdown
    refer to the article titled "A new steganographic algorithm for hiding messages in music"
    and published in Information & Security: An International Journal (e-ISSN 1314-2119,
    https://infosec-journal.com/). Once the paper will be available online it will be linked
    in the README.

    Please note that for more detailed method docs refer to method-level docstring, i.e.:
    help(SteganoMusic.some_method_from_this_class)

    Attributes
    ----------
    music_generator : MusicGenerator
        Sigurdur's code, but modified to be a class. It is essentially an engine for creating
        music using machine learning.

    right_hand : list
        List of all notes played "played by right hand on the piano". Basically those are the
        notes that are presented using only one letter and one number, for example: "A3". All
        of those notes are used for steganographic purposes.

    ones : list
        List of notes (right_hand style) used to represent a binary '1'.

    zeros : list
        List of notes (right_hand style) used to represent a binary '0'.

    note_map : dict
        Just for simplicity of usage in the code it is a dict with ones and zeros as values and
        1 and 0 (integers) as keys.

    Methods
    -------
    save_to_midi():
        Wrapper method for MusicGenerator.create_midi. It saves the output of the
        generate_stegano_notes() as the MIDI file.

    tester(iters, secret, depth):
        Method for testing algorithm. It tries iters times to hide secret message using
        every depth Note.

    create_one_zero_sets():
        Creates two lists, one representing all Notes that will be mapped to 1 and second all
        Notes that will be mapped to 0. Then it creates a dict so that those lists are easily
        accessible by integer keys. This behaviour is not in __init__ because ultimately this
        algorithm should allow to create different sets and be able to send that information
        somehow (that is a TODO).

    _convert_to_bits(message):
        Given ASCII string it returns a string of 1s and 0s representing that string.

    _convert_to_string(bit_message):
        Given a string of 1s and 0s it converts each byte in that string to ASCII character and
        returns new string.

    _is_chord(note_or_chord):
        Return True if given string is a Chord and False if it is a Note.

    _create_todo_dict(bit_message):
        Given a string of 1s and 0s it creates a dict with steps. Keys are integers and values
        are characters from given string. Essentially 'key' is an index of a 'value' in the
        given string.

    _get_next_best_guess(prediction):
        Sets current argmax to -10 and then calculates new argmax (which will be next best
        guess. Then it converts it to note string (int_to_note) and returns that string.

    _check_intersection(list1, list2):
        Given two lists as parameters it checks if there are common elements int them. If there
        are then it returns True, if not then it returns False.

    _create_list_of_pitchnames(chord_or_note):
        Just a helper method. It converts chord_or_note to the list of note names.
        If chord_or_note is a Note that list's length will be 1.

    _check_if_last_three_notes_are_the_same(prediction_output, candidate):
        Given the prediction output and candidate note for appending it returns true if all of
        them are the same note (nameWithOctave wise).

    generate_stegano_notes(secret, depth):
        Creates a list of Notes/Chords that will be converted to midi file. This is essentially
        MusicGenerator.generate_notes() but with extra steps to encode secret message.

    create_stegano_file(secret, depth, filename):
        Wrapper method for creating steganographic MIDI sequence and then saving it as MIDI
        file.

    _rewrite_list_with_duplicates(to_rewrite, how_many, index_for_multi_rewrite):
        Helper method for sanitizing midi sequence. It creates the same list as input.
        But inserts how_many times object at index_for_multi_rewrite index.

    _check_if_rewrite_needed(notes):
        Helper method for sanitization purposes. It checks if a rewrite of the list
        is necessary.

    _check_if_chords_were_split(notes):
        Helper method. Checks if a chord was split into two notes of the same offset.

    _rewrite_list_and_compile_chord(to_rewrite, of, indices):
        If there are chords that were split into notes then they need to be combined back into
        a chord.

    _delete_not_valid_duration_experiment(notes):
        This method deletes anything with duration.quarterLength not equal to 1.0. This is
        an experiment (that seems to be working). It is invoked after all sanitization.

    sanitize_midi_sequence_for_extraction(notes):
        Method that wraps all the helper methods in order to produce sanitized list. If there
        are multiple same Notes next to each other they will be 'compiled' into one note in
        midi file. That is why we need to run this method if we want to retrieve original list.

    get_names_list(notes):
        Simple method for converting list of Notes and Chords to list of string representation
        of their pitches. For note it is simply it's pitch. For Chord it is a list of pitches
        in that Chord.

    def fetch_secret_message(self, file: str, depth=8) -> str:
        Method extracting secret message."""

    def __init__(self):
        self.music_generator = MusicGenerator()
        self.create_one_zero_sets()

    def save_to_midi(self, midi_sequence: list, filename=""):
        """Wrapper method for MusicGenerator.create_midi. It saves the output of the
        generate_stegano_notes() as the MIDI file.

        Parameters
        ----------
        midi_sequence : list
            A list of midi notes to be converted into MIDI file.

        filename : str, optional
            A filename of a newly created MIDI file. If no filename is provided then the file
            will be called 'test_output.mid'"""

        self.music_generator.create_midi(midi_sequence, filename=filename)

    def tester(self, iters: int, secret: str, depth: int):
        """Method for testing algorithm. It tries iters times to hide secret message using
        every depth Note.

        Parameters
        ----------
        iters : int
            Number of times an algorithm will be checked.

        secret : str
            A secret message used during testing

        depth : int
            Hiding depth value used during testing.

        Notes
        -----
        Keep in mind that in case there is an error, i.e.: it was not possible to retrieve the
        secret message this method will save that problematic sequence to a "ERROR_to_midi.txt"
        file and will quit (no next iterations)."""

        for _ in tqdm(range(iters)):
            to_midi = self.generate_stegano_notes(secret, depth)
            self.save_to_midi(to_midi)
            o = self.fetch_secret_message("test_output.mid", depth)
            if o != secret:
                print("ERROR, saving problematic midi sequence!")
                with open("ERROR_to_midi.txt", "w") as f:
                    for seq in to_midi:
                        f.write("".join([seq, "\n"]))
                break

    def create_one_zero_sets(self):
        """Creates two lists, one representing all Notes that will be mapped to 1 and second all
        Notes that will be mapped to 0. Then it creates a dict so that those lists are easily
        accessible by integer keys. This behaviour is not in __init__ because ultimately this
        algorithm should allow to create different sets and be able to send that information
        somehow (that is a TODO)."""

        self.right_hand = [
            note
            for note in self.music_generator.int_to_note.values()
            if "." not in note and not note.isdigit()
        ]

        self.ones = [
            self.right_hand[index]
            for index in range(len(self.right_hand))
            if index % 2 == 0
        ]
        self.zeros = [
            self.right_hand[index]
            for index in range(len(self.right_hand))
            if index % 2 == 1
        ]

        self.note_map = {0: self.zeros, 1: self.ones}

    def _convert_to_bits(self, message: str) -> str:
        """Given ASCII string it returns a string of 1s and 0s representing that string.

        Parameters
        ----------
        message : str
            ASCII string representing a message to be converted into bitstring.

        Returns
        -------
        bitstring : str
            String consisting of only '1' and '0' and representing a given message.

        Notes
        -----
        If you want to convert bitstring into ASCII string feel free to use _convert_to_string()
        """

        result = []
        for c in message:
            bits = bin(ord(c))[2:]
            bits = "00000000"[len(bits) :] + bits
            result.append(bits)
        return "".join(result)

    def _convert_to_string(self, bit_message: str) -> str:
        """Given a string of 1s and 0s it converts each byte in that string to ASCII character
        and returns new string.

        Parameters
        ----------
        bit_message : str
            String consisting of only '1' and '0' and representing an ASCII message.

        Returns
        -------
        message : str
            ASCII string representing a message.

        Notes
        -----
        If you want to convert the result back into bitstring feel free to use
        _convert_to_bits()."""

        chars = []
        for b in range(int(len(bit_message) / 8)):
            byte = bit_message[b * 8 : (b + 1) * 8]
            chars.append(chr(int("".join([str(bit) for bit in byte]), 2)))

        return "".join(chars)

    def _is_chord(self, note_or_chord: str) -> bool:
        """Return True if given string is a Chord and False if it is a Note.

        Parameters
        ----------
        note_or_chord : str
            String representation of a sound.

        Returns
        -------
        result : bool
            If note_or_chord represents a chord then it returns True else it returns False."""

        return (note_or_chord.isdigit()) or ("." in note_or_chord)

    def _create_todo_dict(self, bit_message: str) -> dict:
        """Given a string of 1s and 0s it creates a dict with steps. Keys are integers
        and values are characters from given string. Essentially 'key' is an index of a
        'value' in the given string.

        Parameters
        ----------
        bit_message : str
            Bitstring message to be hidden.

        Returns
        -------
        result : dict
            A dictionary with steps to hide.

        Notes
        -----
        Result is basically a int<->int pair, key is the consecutive number of step and value
        is a bit to hide."""

        return dict(list(enumerate([int(s) for s in bit_message])))

    def _get_next_best_guess(self, prediction) -> Tuple[str, np.int64]:
        """Sets current argmax to -10 and then calculates new argmax (which will be next best
        guess. Then it converts it to note string (int_to_note) and returns that string.

        Parameters
        ----------
        prediction
            TensorFlow prediction.

        Returns
        -------
        result : Tuple[str, np.int64]
            Tuple with next best guess and index of that guess."""

        current_argmax_position = np.where(prediction == np.amax(prediction))
        current_argmax_x = current_argmax_position[0][0]
        current_argmax_y = current_argmax_position[1][0]
        prediction[current_argmax_x][current_argmax_y] = -100
        new_index = np.argmax(prediction)
        return self.music_generator.int_to_note[new_index], new_index

    def _check_intersection(self, list1: list, list2: list) -> bool:
        """Given two lists as parameters it checks if there are common elements int them.
        If there are then it returns True, if not then it returns False.

        Parameters
        ----------
        list1 : list,
        list2 : list
            Two lists to be checked.

        Returns
        -------
        Returns True if there are common elements in a given list."""

        list1_set = set(list1)
        inter = list1_set.intersection(list2)
        if len(inter) > 0:
            return True

        return False

    def _create_list_of_pitchnames(self, chord_or_note: str) -> list:
        """Just a helper method. It converts chord_or_note to the list of note names.
        If chord_or_note is a Note that list's length will be 1.

        Parameters
        ----------
        chord_or_note : str
            String representing either a chord or a note.

        Returns
        -------
        pitchnames : list
            List of pitchnames that create a given chord_or_note."""

        if self._is_chord(chord_or_note):
            return [note.Note(int(n)).pitch.name for n in chord_or_note.split(".")]
        else:
            return [note.Note(chord_or_note).pitch.name]

    def _check_if_last_three_notes_are_the_same(
        self, prediction_output: list, candidate: str
    ) -> bool:
        """Given the prediction output and candidate note for appending it returns true if
        all of them are the same note (nameWithOctave wise).

        Parameters
        ----------
        prediction_output : list
            List of MIDI sequence already on the list to be converted into MIDI file.

        candidate : str
            A new MIDI note, candidate to be added to the prediction_output.

        Returns
        -------
        result : bool
            It returns True if candidate and two last sounds are all the same chord. Otherwise
            it returns False."""

        if (
            self._is_chord(candidate)
            or self._is_chord(prediction_output[-1])
            or self._is_chord(prediction_output[-2])
        ):
            # If either is a chord then we are good
            return False

        name1 = note.Note(candidate).pitch.nameWithOctave
        name2 = note.Note(prediction_output[-1]).pitch.nameWithOctave
        name3 = note.Note(prediction_output[-2]).pitch.nameWithOctave

        if name1 == name2 and name2 == name3:
            return True

        return False

    def generate_stegano_notes(self, secret: str, depth=8) -> list:
        """Creates a list of Notes/Chords that will be converted to midi file.
        This is essentially MusicGenerator.generate_notes() but with extra steps
        to encode secret message.

        Parameters
        ----------
        secret : str
            ASCII string representing a secret message to be hidden.

        depth : int, optional
            Hiding depth value, by default it is set to 8.

        Returns
        -------
        prediction_output : list
            MIDI sequence to be converted into MIDI file.

        Notes
        -----
        This method is not written in an optimal manner :(. I do realize that there is some
        duplicate code here, but the deadlines where coming close and I needed to resolve the
        issues. Refactoring is needed. Those issues are connected to some *weird* things here.
        Due to the machine learning model and converting to MIDI file is handled by external
        projects/modules I had to implement some workaround (for example there cannot be three
        same chords in a sequence, because there will be issues with retrieving secret message.
        """

        secret_bits = self._convert_to_bits(secret)
        steps = self._create_todo_dict(secret_bits)
        finished = False

        start = np.random.randint(0, len(self.music_generator.network_input) - 1)
        pattern = self.music_generator.network_input[start]

        prediction_output = []

        index_to_be_encoded = 0
        step_index = 0

        with open("stegano_test.txt", "w") as test_file:
            # stegano_test.txt is a debug file.
            while not finished:
                # creating a log line for debug purposes
                line_to_write: str = ""

                prediction_input = np.reshape(pattern, (1, len(pattern), 1))
                prediction_input = prediction_input / float(
                    self.music_generator.n_vocab
                )

                prediction = self.music_generator.model.predict(
                    prediction_input, verbose=0
                )

                index = np.argmax(prediction)
                result: str = self.music_generator.int_to_note[index]

                line_to_write = "".join([line_to_write, f"calculated: {result} | "])

                while result.isdigit():
                    # If this is a single digit then it will be converted to Chord containing
                    # only one pitch. Unfortunately upon writing to file that Chord is
                    # converted to Note. This will break the algorithm so we need to go with
                    # something else. Idea here is to do the same as in the case when the
                    # calculated Note is not corresponding to bit we want to encode.
                    result, index = self._get_next_best_guess(prediction)

                if len(prediction_output) > 0:
                    if self._is_chord(prediction_output[-1]):
                        # There cannot be the same pitch.name (regardless of octave) in two
                        # consequtive chords OR in the Note following the Chord.
                        notes_in_last_chord = self._create_list_of_pitchnames(
                            prediction_output[-1]
                        )
                        notes_in_result = self._create_list_of_pitchnames(result)
                        while self._check_intersection(
                            notes_in_last_chord, notes_in_result
                        ):
                            result, index = self._get_next_best_guess(prediction)
                            while result.isdigit():
                                result, index = self._get_next_best_guess(prediction)

                            notes_in_result = self._create_list_of_pitchnames(result)
                    else:
                        if self._is_chord(result):
                            # There also cannot be same pitch.name (regardless of Octave) in
                            # the Chord following the Note
                            notes_in_last_element = self._create_list_of_pitchnames(
                                prediction_output[-1]
                            )
                            notes_in_result = self._create_list_of_pitchnames(result)
                            while self._check_intersection(
                                notes_in_result, notes_in_last_element
                            ):
                                result, index = self._get_next_best_guess(prediction)
                                while result.isdigit():
                                    result, index = self._get_next_best_guess(
                                        prediction
                                    )

                                notes_in_result = self._create_list_of_pitchnames(
                                    result
                                )

                        if len(prediction_output) >= 2:
                            # checking if there are at least 3 elements in output
                            if not self._is_chord(prediction_output[-2]):
                                # checking if last two elements were Notes
                                if not self._is_chord(result):
                                    # If there are 3 consecutive Notes we need to check if they
                                    # all have the same nameWithOctave. There can be two Notes
                                    # next to each other with the same nameWithOctave, but
                                    # not three
                                    if (
                                        note.Note(result).pitch.nameWithOctave
                                        == note.Note(
                                            prediction_output[-1]
                                        ).pitch.nameWithOctave
                                        and note.Note(result).pitch.nameWithOctave
                                        == note.Note(
                                            prediction_output[-2]
                                        ).pitch.nameWithOctave
                                    ):
                                        # If we have three Notes of the same nameWithOctave
                                        # we change the last one. No need for a while loop,
                                        # because getting next best guess cannot return same
                                        # note.
                                        result, index = self._get_next_best_guess(
                                            prediction
                                        )
                                        while result.isdigit():
                                            result, index = self._get_next_best_guess(
                                                prediction
                                            )

                if self._is_chord(result):
                    # If it is a Chord then we add it. Message is encoded only on Notes

                    prediction_output.append(result)
                    pattern.append(index)
                    pattern = pattern[1 : len(pattern)]
                    line_to_write = "".join([line_to_write, f"added: {result}"])
                else:
                    if index_to_be_encoded < 2 * depth or (
                        not index_to_be_encoded % depth == 0
                    ):
                        # First 2 * depth Notes are also added
                        # After 2 * depth first Notes only every depth Note is related to
                        # secret message
                        prediction_output.append(result)
                        pattern.append(index)
                        pattern = pattern[1 : len(pattern)]
                        line_to_write = "".join([line_to_write, f"added: {result}"])
                    else:
                        # This is when Note will reflect our message
                        bit_to_encode = steps[step_index]
                        line_to_write = "".join(
                            [line_to_write, f"to_encode: {bit_to_encode} | "]
                        )
                        if result in self.note_map[bit_to_encode]:
                            # Great, prediction will map to the bit we want
                            # proceeding as if nothing is needed
                            prediction_output.append(result)
                            pattern.append(index)
                            pattern = pattern[1 : len(pattern)]
                            line_to_write = "".join([line_to_write, f"added: {result}"])
                        else:
                            # We need to find the most suitable next candidate
                            # We simply set the current argmax value to something low
                            # (like -10). Then we get the next argmax and if it is suitable
                            # then we add that one print("trying something different")
                            candidate = "---N/A---"
                            note_in_last_chord = False
                            while (
                                candidate not in self.note_map[bit_to_encode]
                                or self._check_if_last_three_notes_are_the_same(
                                    prediction_output, candidate
                                )
                                or note_in_last_chord
                            ):
                                candidate, new_index = self._get_next_best_guess(
                                    prediction
                                )
                                notes_in_last_element = self._create_list_of_pitchnames(
                                    prediction_output[-1]
                                )
                                notes_in_candidate = self._create_list_of_pitchnames(
                                    candidate
                                )
                                note_in_last_chord = self._check_intersection(
                                    notes_in_last_element, notes_in_candidate
                                )

                            # print("Found replacement")
                            prediction_output.append(candidate)
                            pattern.append(new_index)
                            pattern = pattern[1 : len(pattern)]
                            line_to_write = "".join(
                                [line_to_write, f"added: {candidate}"]
                            )

                        step_index += 1

                    index_to_be_encoded += 1

                # last workarounds
                if step_index == len(secret_bits):
                    # we need to check if last two Notes are the same. If they are then we need
                    # to change that, because sanitization won't detect it.
                    if prediction_output[-1] == prediction_output[-2]:
                        # problem, we need to change that
                        print("Last two Notes are the same")
                        candidate = "---N/A---"
                        while candidate not in self.note_map[steps[step_index - 1]]:
                            print("Changing")
                            candidate, index = self._get_next_best_guess(prediction)
                        print("Changed")
                        prediction_output[-1] = candidate
                        line_to_write = "".join(
                            [line_to_write, f" | same last notes | added {candidate}"]
                        )
                    finished = True

                test_file.write("".join([line_to_write, "\n"]))

        return prediction_output

    def create_stegano_file(self, secret: str, hiding_depth=8, filename=""):
        """Wrapper method for creating steganographic MIDI sequence and then saving it as MIDI
        file.

        Parameters
        ----------
        secret : str
            Secret message to be hidden.

        hiding_depth : int, optional
            Hiding depth, by default it is set to 8.

        filename : str, optional
            Filename to use when creating a MIDI file. By default the file created will be
            called 'test_output.mid'.

        Returns
        -------
        None. It creates the file on the disk and prints 'done' when it is done."""

        midi_seq = self.generate_stegano_notes(secret, hiding_depth)
        self.save_to_midi(midi_seq, filename)
        print("done")

    def _rewrite_list_with_duplicates(
        self, to_rewrite: list, how_many: int, index_for_multi_rewrite: int
    ) -> list:
        """Helper method for sanitizing midi sequence. It creates the same list as input, but
        inserts how_many times object at index_for_multi_rewrite index.

        Parameters
        ----------
        to_rewrite : list
            List to be modified.

        how_many : int
            How many times a specific objects needs to be added inside the list.

        index_for_multi_rewrite : int
            Index of an object to be added multiple times to the list.

        Returns
        -------
        rewritten : list
            A rewritten list which is almost exactly the same as to_rewrite list, however it
            contains a specific object more than once."""

        i = 0
        rewritten = []
        while i < len(to_rewrite):
            if i != index_for_multi_rewrite:
                # not yet at the desired index so just add elements as they are
                rewritten.append(to_rewrite[i])
            else:
                for n in range(how_many):
                    # new_note needs to be created
                    new_note = copy.deepcopy(to_rewrite[i])
                    new_note.offset = to_rewrite[i].offset + 0.5 * n
                    new_duration = duration.Duration()
                    new_duration.quarterLength = 1.0
                    new_note.duration = new_duration
                    rewritten.append(new_note)
            i += 1

        return rewritten

    def _check_if_rewrite_needed(self, notes: list) -> Tuple[bool, int, int]:
        """Helper method for sanitization purposes. It checks if a rewrite of the list
        is necessary.

        Parameters
        ----------
        notes : list
            List of notes that might need rewriting.

        Returns
        -------
        result : Tuple[bool, int, int]
            If the rewrite is needed then bool value is True and second part of Tuple is the
            index of Note that needs a rewrite, third is how many times it should be
            "untangled". If rewrite is not needed then bool value is False and both integers
            are set to -1."""

        for i in range(len(notes) - 1):
            difference = notes[i + 1].offset - notes[i].offset
            if difference > 0.5:
                # In the MIDI file creation process the delta duration is equal to 0.5 and if
                # that won't be the case then it means that music21 module has 'optimized' and
                # created one longer note instead of few same and shorter notes. That is why
                # rewriting is sometimes needed
                how_many = int(difference / 0.5)
                return True, i, how_many

        return False, -1, -1

    def _check_if_chords_were_split(self, notes: list) -> Tuple[bool, int, list]:
        """Helper method. Checks if a chord was split into two notes of the same offset.

        Parameters
        ----------
        notes : list
            List of notes that might contain chords that were split into notes of the same
            offset.

        Returns
        -------
        result : Tuple[bool, int, list]
            If there is a chord that was split into notes then bool value is set to True, int
            will be an offset value and list will contain indices of where that offset is
            present. Otherwise the bool will be False, int will be set to -1 and list will
            contain only one element of -1."""

        d = {}
        for n in range(len(notes)):
            if notes[n].offset in d:
                d[notes[n].offset].append(n)
            else:
                d[notes[n].offset] = [n]

        for key in d.keys():
            if len(d[key]) > 1:
                return True, key, d[key]

        return False, -1, [-1]

    def _rewrite_list_and_compile_chord(
        self, to_rewrite: list, of: float, indices: list
    ) -> list:
        """If there are chords that were split into notes then they need to be combined back
        into chord.

        Parameters
        ----------
        to_rewrite : list
            List that needs a rewrite.

        of : float
            An offset that the compiled chord should have.

        indices : list
            A list containing indices of notes from to_rewrite list that are supposed to be
            one chord.

        Returns
        -------
        rewrite : list
            A rewritten list which is almost exactly the same as to_rewrite list, however it
            contains a chord instead of notes that were supposed to create one."""

        rewrite = []

        compiled_chord = chord.Chord([to_rewrite[i] for i in indices])
        compiled_chord.offset = of
        new_duration = duration.Duration()
        new_duration.quarterLength = 1.0
        compiled_chord.duration = new_duration

        index = 0
        while index < len(to_rewrite):
            if index in indices:
                # this is when we insert our new Chord
                rewrite.append(compiled_chord)
                index += len(indices)
            else:
                rewrite.append(to_rewrite[index])
                index += 1

        return rewrite

    def _delete_not_valid_duration_experiment(self, notes: list) -> list:
        """This method deletes anything with duration.quarterLength not equal to 1.0. This is
        an experiment (that seems to be working). It is invoked after all sanitization.

        Parameters
        ----------
        notes : list
            List of notes that may or may not contain invalid notes.

        Returns
        ------
        result : list
            It is the same list as the input with deleted elements that have invalid duration
            of duration.quarterLength equal to 1.0.

        Notes
        -----
        This method is potentially not needed, however it does not hurt to invoke it just in
        case."""

        return [nt for nt in notes if nt.duration.quarterLength == 1.0]

    def sanitize_midi_sequence_for_extraction(self, notes: list) -> list:
        """Method that wraps all the helper methods in order to produce sanitized list.
        If there are multiple same Notes next to each other they will be 'compiled' into
        one note in midi file. That is why we need to run this method if we want to retrieve
        original list.

        Parameters
        ----------
        notes : list
            List of notes that is supposed to be sanitized.

        Returns
        -------
        notes : list
            Sanitized list that was given as an input."""

        # Checking if there were chords split into separate notes. Those will have the same
        # offset
        finished = False
        while not finished:
            res = self._check_if_chords_were_split(notes)
            if res[0]:
                notes = self._rewrite_list_and_compile_chord(notes, res[1], res[2])
            else:
                finished = True

        # If there were two same notes next to each other they will be compiled into one longer
        # Note in the midi file.
        finished = False
        while not finished:
            res = self._check_if_rewrite_needed(notes)
            if res[0]:
                notes = self._rewrite_list_with_duplicates(notes, res[2], res[1])
            else:
                finished = True

        # EXPERIMENT
        # After all sanitization if there will be anything with duration.quaterLength != 1.0
        # those objects need to be deleted.
        notes = self._delete_not_valid_duration_experiment(notes)

        return notes

    def get_names_list(self, notes: list) -> list:
        """Simple method for converting list of Notes and Chords to list of string representation
        of their pitches. For note it is simply its pitch. For Chord it is a list of pitches in
        that Chord.

        Parameters
        ----------
        notes : list
            List of notes to be converted into list of pitchanmes.

        Returns
        -------
        res : list
            List of pitchnames."""

        res = []
        for n in notes:
            if isinstance(n, chord.Chord):
                pitches_in_chord = [pitch.nameWithOctave for pitch in n.pitches]
                res.append(pitches_in_chord)
            else:
                res.append(n.pitch.nameWithOctave)

        return res

    def fetch_secret_message(self, file: str, depth=8) -> str:
        """Method for extracting secret message.

        Parameters
        ----------
        file : str
            Name of the MIDI file which contains hidden message.

        depth : int, optional
            Hiding depth value. By default it is set to 8.

        Returns
        -------
        result : str
            The secret message hidden inside a file (or some random bytes if there was nothing
            hidden)."""

        midi_analyser = MidiHelper(file)
        notes = midi_analyser.get_midi_notes_list()
        self.note_names_sanitized = self.get_names_list(
            self.sanitize_midi_sequence_for_extraction(notes)
        )

        index_encoded = 0

        out_bit = ""

        to_be_decoded = []

        with open("stegano_decode.txt", "w") as f:
            # stegano_decode.txt is a debug file.
            for note in self.note_names_sanitized:
                # line_to_write is a log line
                line_to_write: str = f"{note} | "
                if isinstance(note, list):
                    # This is a chord, we skip that one
                    line_to_write = "".join([line_to_write, f"chord found | "])
                    f.write("".join([line_to_write, "\n"]))
                    continue
                if index_encoded >= 2 * depth and index_encoded % depth == 0:
                    line_to_write = "".join([line_to_write, f"int. note | "])
                    to_be_decoded.append(note)
                    if note in self.ones:
                        out_bit = "".join([out_bit, "1"])
                        line_to_write = "".join([line_to_write, f"1"])
                    elif note in self.zeros:
                        out_bit = "".join([out_bit, "0"])
                        line_to_write = "".join([line_to_write, f"0"])
                    else:
                        # Obviously we should not end up here
                        print("Something is wrong. Unknown note!")

                index_encoded += 1
                f.write("".join([line_to_write, "\n"]))

        return self._convert_to_string(out_bit)

""" This module generates notes for a midi file using the
    trained neural network
    It is Sigurdur's code but modified to use classes."""
import pickle

import numpy
from keras.layers import Activation
from keras.layers import BatchNormalization as BatchNorm
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.models import Sequential
from music21 import instrument, note, stream, chord


class MusicGenerator:
    def __init__(self):
        with open("../data/notes", "rb") as filepath:
            self.notes = pickle.load(filepath)

        self.pitchnames = sorted(set(item for item in self.notes))
        self.n_vocab = len(set(self.notes))

        self.network_input, self.normalized_input = self.prepare_sequences(
            self.notes, self.pitchnames, self.n_vocab
        )

        self.int_to_note = dict(
            (number, note) for number, note in enumerate(self.pitchnames)
        )

        self.model = self.create_network(self.normalized_input, self.n_vocab)

    def generate(self):
        """ Generate a piano midi file """
        # load the notes used to train the model
        # with open("data/notes", "rb") as filepath:
        #    self.notes = pickle.load(filepath)

        # Get all pitch names
        # self.pitchnames = sorted(set(item for item in notes))
        # Get all pitch names
        # n_vocab = len(set(notes))

        # network_input, normalized_input = prepare_sequences(notes, pitchnames, n_vocab)
        # int_to_note = dict((number, note) for number, note in enumerate(pitchnames))
        # with open("int_to_note.txt", "w") as f:
        #   f.write(f"int_to_note: {int_to_note}")

        # model = create_network(normalized_input, n_vocab)

        prediction_output = self.generate_notes(self.model, self.network_input, self.pitchnames, self.n_vocab)

        self.create_midi(prediction_output)

    def prepare_sequences(self, notes, pitchnames, n_vocab):
        """ Prepare the sequences used by the Neural Network """
        # map between notes and integers and back
        note_to_int = dict((note, number) for number, note in enumerate(pitchnames))

        sequence_length = 100
        network_input = []
        output = []
        for i in range(0, len(notes) - sequence_length, 1):
            sequence_in = notes[i : i + sequence_length]
            sequence_out = notes[i + sequence_length]
            network_input.append([note_to_int[char] for char in sequence_in])
            output.append(note_to_int[sequence_out])

        n_patterns = len(network_input)

        # reshape the input into a format compatible with LSTM layers
        normalized_input = numpy.reshape(network_input, (n_patterns, sequence_length, 1))
        # normalize input
        normalized_input = normalized_input / float(n_vocab)

        return (network_input, normalized_input)

    def create_network(self, network_input, n_vocab):
        """ create the structure of the neural network """
        model = Sequential()
        model.add(
            LSTM(
                512,
                input_shape=(network_input.shape[1], network_input.shape[2]),
                recurrent_dropout=0.3,
                return_sequences=True,
            )
        )
        model.add(LSTM(512, return_sequences=True, recurrent_dropout=0.3,))
        model.add(LSTM(512))
        model.add(BatchNorm())
        model.add(Dropout(0.3))
        model.add(Dense(256))
        model.add(Activation("relu"))
        model.add(BatchNorm())
        model.add(Dropout(0.3))
        model.add(Dense(n_vocab))
        model.add(Activation("softmax"))
        model.compile(loss="categorical_crossentropy", optimizer="rmsprop")

        # Load the weights to each node
        model.load_weights("weights.hdf5")

        return model

    def generate_notes(self, model, network_input, pitchnames, n_vocab, number_of_notes):
        """ Generate notes from the neural network based on a sequence of notes """
        # pick a random sequence from the input as a starting point for the prediction
        start = numpy.random.randint(0, len(network_input) - 1)

        int_to_note = dict((number, note) for number, note in enumerate(pitchnames))

        pattern = network_input[start]
        prediction_output = []

        # generate number_of_notes notes
        for note_index in range(number_of_notes):
            prediction_input = numpy.reshape(pattern, (1, len(pattern), 1))
            prediction_input = prediction_input / float(n_vocab)

            prediction = model.predict(prediction_input, verbose=0)

            index = numpy.argmax(prediction)
            result = int_to_note[index]

            prediction_output.append(result)
            pattern.append(index)
            pattern = pattern[1 : len(pattern)]

        return prediction_output

    def create_midi(self, prediction_output, filename="test_output.mid"):
        """ convert the output from the prediction to notes and create a midi file
            from the notes """
        offset = 0
        output_notes = []

        # create note and chord objects based on the values generated by the model
        for pattern in prediction_output:
            # pattern is a chord
            if ("." in pattern) or pattern.isdigit():
                notes_in_chord = pattern.split(".")
                notes = []
                for current_note in notes_in_chord:
                    new_note = note.Note(int(current_note))
                    new_note.storedInstrument = instrument.Piano()
                    notes.append(new_note)
                new_chord = chord.Chord(notes)
                new_chord.offset = offset
                output_notes.append(new_chord)
            # pattern is a note
            else:
                new_note = note.Note(pattern)
                new_note.offset = offset
                new_note.storedInstrument = instrument.Piano()
                output_notes.append(new_note)

            # increase offset each iteration so that notes do not stack
            offset += 0.5

        midi_stream = stream.Stream(output_notes)
        if filename == "":
            filename = "test_output.mid"

        midi_stream.write("midi", fp=filename)

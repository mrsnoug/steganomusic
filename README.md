# SteganoMusic project

This project is the basis of that research paper: <https://isij.eu/article/new-steganographic-algorithm-hiding-messages-music>

Article version linked above seems to have blurry images, so I uploaded a LaTeX version to this repository. [Here it is](./article.pdf "LaTeX version of article")

It is the project that implements new way of hiding messages in music files. Namely - it hides the messages IN the music itself (using Notes).
It was supposed to be my bachelor degree project, but it escalated into research paper.

For music generation it uses:
<https://github.com/Skuldur/Classical-Piano-Composer>, that is a really great project!

Read: <https://towardsdatascience.com/how-to-generate-music-using-a-lstm-neural-network-in-keras-68786834d4c5>, this article explains how the music generation works (project above)

It uses Python 3.7. However if you don't want to use GUI then you can use Python 3.8 (comment out kivy dependencies from requirements.txt first).

Interpreter usage:
```python3
from analyser import SteganoMusic

# Hiding messages
s = SteganoMusic()
message = "Your secret message"
hiding_depth = 8 
# 8 is default. Feel free to use different one. Keep in mind that bigger number means longer piece of music (and potentially better quality as well)
filename = "completely_normal_file.mid" # or something less suspicious :)
s.create_stegano_file(message, hiding_depth, filename)

# Retrieving secret message
s.fetch_secret_message(filename, hiding_depth) # this returns the string with fetched message
```

Improvements needed:

- dedicated music generation engine
    - Skuldur's project uses music21 module, which is great, but sometimes it "optimizes" internally which make it really hard to retrieve secret messages. That is the reason for some weird limitations in code (for example three same chords in a row is a big "no no")
    - I pretty much butchered Skuldur's project and used it not in the way it was designed to. If there would be a model that is designed to operate in the way I am using Skuldur's then I believe the quality (enjoyability) would increase
- A way of transmitting the info on how the notes were split into '1'- and '0'-sets. For now those are split arbitrarily. It is possible to split notes into different sets (algorithm will work as long as notes are in the same sets during hiding and retrieving message), however the receiver needs to know that division. I would like it to send that info in the file as well. CURRENT IDEA: Have the first 'section' of the file always use same hiding depth in which the sender would mark which set division was used. That would require both sender and receiver to have some kind of database of previously agreed sets. It is **some** kind of solution, but it is not much better than what it is right now.
- Code refactoring, there is some duplicate code and I don't like that. When I'll find some spare time I plan to fix it.

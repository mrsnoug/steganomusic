import kivy
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.textinput import TextInput

from analyser import SteganoMusic

kivy.require("1.10.1")


class ModePickerPage(GridLayout):
    description = """Chose the Mode you want to use."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 2

        self.add_widget(Label(text=self.description))

        self.bottom_line = GridLayout(cols=2)

        self.hide_message = Button(text="Hide")
        self.hide_message.bind(on_press=self.hide_message_handler)

        self.extract_message = Button(text="Extract")
        self.extract_message.bind(on_press=self.extract_message_handler)

        self.bottom_line.add_widget(self.hide_message)
        self.bottom_line.add_widget(self.extract_message)

        self.add_widget(self.bottom_line)

    def hide_message_handler(self, instance):
        stegano_app.screen_manager.current = "HideMode"
        stegano_app.resize_window((800, 600))

    def extract_message_handler(self, instance):
        stegano_app.screen_manager.current = "ExtractMode"
        stegano_app.resize_window((800, 600))


class HideModePage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 2
        self.rows = 4

        # Row regarding secret message
        self.add_widget(Label(text="Secret:"))
        self.secret = TextInput(multiline=False)
        self.add_widget(self.secret)

        # Row regarding depth
        self.add_widget(Label(text="Hiding depth:"))
        self.depth = TextInput(multiline=False)
        self.add_widget(self.depth)

        # Row regarding file name
        self.add_widget(Label(text="Filename:"))
        self.filename = TextInput(multiline=False)
        self.add_widget(self.filename)

        self.go_back_button = Button(text="Go back")
        self.go_back_button.bind(on_press=self.go_back_button_handler)
        self.add_widget(self.go_back_button)
        self.generate_button = Button(text="Generate")
        self.generate_button.bind(on_press=self.generate_button_handler)
        self.add_widget(self.generate_button)

    def generate_button_handler(self, instance):
        secret_message = self.secret.text
        hiding_depth = int(self.depth.text)
        midi_filename = self.filename.text

        stegano_app.stegano_music.save_to_midi(
            stegano_app.stegano_music.generate_stegano_notes(
                secret=secret_message, depth=hiding_depth
            ),
            filename=midi_filename,
        )

        stegano_app.info_page.update_info(f"DONE: created {midi_filename} file.")
        stegano_app.screen_manager.current = "Info"

    def go_back_button_handler(self, instance):
        stegano_app.screen_manager.current = "ModePicker"


class ExtractModePage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 2
        self.rows = 3

        self.add_widget(Label(text="filename:"))
        self.filename = TextInput(multiline=False)
        self.add_widget(self.filename)

        self.add_widget(Label(text="Depth:"))
        self.depth = TextInput(multiline=False)
        self.add_widget(self.depth)

        self.go_back_button = Button(text="Go back")
        self.go_back_button.bind(on_press=self.go_back_button_handler)
        self.add_widget(self.go_back_button)

        self.extract_button = Button(text="Extract")
        self.extract_button.bind(on_press=self.extract_button_handler)
        self.add_widget(self.extract_button)

    def go_back_button_handler(self, instance):
        stegano_app.screen_manager.current = "ModePicker"

    def extract_button_handler(self, instance):
        midi_filename = self.filename.text
        hiding_depth = int(self.depth.text)
        o, l, n = stegano_app.stegano_music.fetch_secret_message(
            file=midi_filename, depth=hiding_depth
        )

        stegano_app.info_page.update_info(f"Secret message: {o}")
        stegano_app.screen_manager.current = "Info"


class InfoPage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 2

        self.info = Label(halign="center", valign="center", font_size=30)
        self.info.bind(width=self.update_text_width)
        self.add_widget(self.info)

        self.go_to_modepicker_button = Button(text="Pick a mode")
        self.go_to_modepicker_button.bind(on_press=self.go_to_modepicker_button_handler)
        self.add_widget(self.go_to_modepicker_button)

    def update_text_width(self, *_):
        self.info.text_size = (self.info.width * 0.9, None)

    def update_info(self, new_info):
        self.info.text = new_info

    def go_to_modepicker_button_handler(self, instance):
        stegano_app.screen_manager.current = "ModePicker"


class SteganoApp(App):
    def build(self):
        self.stegano_music = SteganoMusic()

        self.screen_manager = ScreenManager()

        self.mode_picker_page = ModePickerPage()
        screen = Screen(name="ModePicker")
        screen.add_widget(self.mode_picker_page)
        self.screen_manager.add_widget(screen)

        self.hide_mode_page = HideModePage()
        screen = Screen(name="HideMode")
        screen.add_widget(self.hide_mode_page)
        self.screen_manager.add_widget(screen)

        self.extract_mode_page = ExtractModePage()
        screen = Screen(name="ExtractMode")
        screen.add_widget(self.extract_mode_page)
        self.screen_manager.add_widget(screen)

        self.info_page = InfoPage()
        screen = Screen(name="Info")
        screen.add_widget(self.info_page)
        self.screen_manager.add_widget(screen)

        return self.screen_manager

    def resize_window(self, size_tuple):
        Window.size = size_tuple


if __name__ == "__main__":
    stegano_app = SteganoApp()
    stegano_app.run()

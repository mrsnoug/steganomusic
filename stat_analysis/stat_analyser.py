import copy
import json
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from music21 import chord, duration, midi, note, pitch
from tqdm import tqdm

from musthe import Note as mustheNote
from musthe import Chord as mustheChord
from musthe import Scale as mustheScale

from ..analyser import SteganoMusic


class MidiAnalyser:
    """Yet another class for analysing MIDI files.

    Please note that for more detailed method docs refer to method-level docstring, i.e.:
    help(MidiAnalyser.some_method_from_this_class)

    Methods
    -------
    list_list_instruments():
        Simply prints out instruments found in MIDI file (might be useful for some appliances).

    get_get_midi_notes_list():
        Returns a list of notes (sounds) found in the MIDI file (in the first track).

    perform_normal_file_statistical_analysis():
        Performs a statistical analysis of a regular (no secret message file). It always
        returns the dict with data and by default it also saves it to a file. To visualize
        the data (draw plots) use the draw_plots_for_data() or draw_bigger_plots_for_article()
        methods. If data is saved to a file the filename will be "<midi_filename>.data".

    draw_plots_for_data():
        Draws the plots for a given data (returned by the methods which perform statistical
        analysis).

    draw_bigger_plots_for_article():
        THIS METHOD IS REDUNDANT. IT DOES EXACTLY SAME THING WHAT draw_plots_for_data()
        METHOD DOES. THIS ONE JUST MAKES THE FONT SLIGHTLY BIGGER. IT WAS USED FOR
        SCREENSHOTS CREATION PURPOSES.

    _prepare_data_to_plot():
        It prepares the data for plotting. Basically it is sorting unpacking them. It is
        written to be used internally, in other methods. But it is not like you'll go to jail
        for using it in your own scripts.

    _apply_data_to_plot():
        Helper method to apply a bit of DRY into the code. It simply applies the given data
        to a given subplot.

    perform_stegano_file_statistical_analysis():
        It is very similar to the perform_normal_file_statistical_analysis() method, however
        this also saves/returns the data associated with the steganographic aspects of a file,
        namely: which notes and how many times were they used for steganographic purposes.

    find_correlation():
        Method which calculates autocorrelation. Surprisingly this was not very useful.

    _chunk_generator():
        Helper method for generating list chunks. Used for musical scale determination.

    determine_scale():
        Method which determines how much a scale fits specific MIDI file.

    extract_notes():
        It extracts sounds from MIDI file.
    """

    def __init__(self, midi_file: str):
        mf = midi.MidiFile()
        mf.open(midi_file)
        mf.read()
        mf.close()
        self.base_midi_stream = midi.translate.midiFileToStream(mf)
        self.midi_file_name = midi_file
        self.stegano_music = SteganoMusic()

    def list_instruments(self):
        """Simply prints out instruments found in MIDI file (might be useful for some
        appliances)"""

        part_stream = self.base_midi_stream.parts.stream()
        print("List of instruments found in MIDI file:")
        for p in part_stream:
            print(p.partName)

    def get_midi_notes_list(self):
        """Returns a list of notes (sounds) found in the MIDI file (in the first track)."""

        return [nt for nt in self.base_midi_stream.parts[0].flat.notes]

    def perform_normal_file_statistical_analysis(self, save=True) -> dict:
        """Performs a statistical analysis of a regular (no secret message file). It always
        returns the dict with data and by default it also saves it to a file. To visualize
        the data (draw plots) use the draw_plots_for_data() or draw_bigger_plots_for_article()
        methods. If data is saved to a file the filename will be "<midi_filename>.data".

        Parameters
        ----------
            save : boolean
                If it is set to True then the method will also save the data it returns to
                a file. This parameter is set to True by default.

        Returns
        -------
            stat_data : dict
                A dict containg results.

        Notes
        -----
            The file created by this method is actually a JSON.

            stat_data dict has the following keys:
                * "total_num_sounds" -> total number of sounds in the MIDI file (notes and
                    chords)
                * "notes_num" -> total number of notes in the MIDI file.
                * "chords_num" -> total number of chords in the MIDI file
                * "sound_occurances_map" -> it is a dict with a note name and corresponding
                    number of occurrences of that note (it takes into account only notes)
                * "sound_occurances_map_with_chords" -> it is a dict with a note name and
                    corresponding number of occurrences of that note (it also takes into
                    account notes that are used to create a chord)
                * "chord_occurances_map" -> it is a dict with a chord name and corresponding
                    number of occurrences of that chord"""

        notes = self.get_midi_notes_list()

        total_number_of_sounds = len(notes)
        notes_num = 0
        chords_num = 0

        sound_occurances_map = {}
        sound_occurances_map_with_chords = {}
        chord_occurances_map = {}

        for note in notes:
            if isinstance(note, chord.Chord):
                chords_num += 1
                full_chord_name = []

                for p in note.pitches:
                    # getting pitch/note name
                    name = p.nameWithOctave

                    # createing full chord name
                    full_chord_name.append(name)

                    # adding pitch/note_name to the map with chords
                    if name not in sound_occurances_map_with_chords:
                        sound_occurances_map_with_chords[name] = 1
                    else:
                        sound_occurances_map_with_chords[name] += 1

                # Combining into one string. Using sorted so that they all will be in the same
                # order
                full_chord_name = " ".join(sorted(full_chord_name))

                # adding chord to the map
                if full_chord_name not in chord_occurances_map:
                    chord_occurances_map[full_chord_name] = 1
                else:
                    chord_occurances_map[full_chord_name] += 1
            else:
                notes_num += 1

                name = note.pitch.nameWithOctave
                if name not in sound_occurances_map:
                    sound_occurances_map[name] = 1
                else:
                    sound_occurances_map[name] += 1

                if name not in sound_occurances_map_with_chords:
                    sound_occurances_map_with_chords[name] = 1
                else:
                    sound_occurances_map_with_chords[name] += 1

        stat_data = {
            "total_num_sounds": total_number_of_sounds,
            "notes_num": notes_num,
            "chords_num": chords_num,
            "sound_occurances_map": sound_occurances_map,
            "sound_occurances_map_with_chords": sound_occurances_map_with_chords,
            "chord_occurances_map": chord_occurances_map,
        }

        if save:
            data_filename = ".".join([self.midi_file_name.split(".")[0], "data"])
            with open(data_filename, "w") as data_file:
                json.dump(stat_data, data_file)

        return stat_data

    def draw_plots_for_data(self, data_file: str, is_stegano: bool):
        """Draws the plots for a given data (returned by the methods which perform statistical
        analysis).

        Parameters
        ----------
        data_file : str
            Name of the file which contains the data.

        is_stegano : bool
            A boolean value representing whether the data to be plotted is associated with the
            stegano MIDI file.

        Returns
        -------
        None. It displays a matplotlib.plot."""

        with open(data_file, "r") as f:
            data = json.load(f)

        if is_stegano:
            # if the data is associated with stegano MIDI file then there will be one more plot
            # representing which notes were used for steganographic purposes.
            fig, axs = plt.subplots(5, 1)
        else:
            fig, axs = plt.subplots(4, 1)

        names = ["total", "notes", "chord"]
        vals = [data["total_num_sounds"], data["notes_num"], data["chords_num"]]

        axs[0].bar(names, vals)
        axs[0].set_ylabel("Notes/Chords num")

        names, vals, x = self._prepare_data_to_plot(data["sound_occurances_map"])
        self._apply_data_to_plot(names, vals, x, axs, 1, "Sound occurances")

        names, vals, x = self._prepare_data_to_plot(
            data["sound_occurances_map_with_chords"]
        )
        self._apply_data_to_plot(names, vals, x, axs, 2, "Sound Occurances /w chords")

        if data["chord_occurances_map"] != {}:
            names, vals, x = self._prepare_data_to_plot(data["chord_occurances_map"])
            self._apply_data_to_plot(names, vals, x, axs, 3, "Chord occurances")

        if is_stegano:
            names, vals, x = self._prepare_data_to_plot(data["stegano_notes_data"])
            self._apply_data_to_plot(names, vals, x, axs, 4, "Stegano Data")

        plt.show()

    def draw_bigger_plots_for_article(self, data_file: str, is_stegano: bool):
        """THIS METHOD IS REDUNDANT. IT DOES SIMILAR THING TO WHAT draw_plots_for_data() METHOD
        DOES. THIS ONE JUST MAKES THE FONT SLIGHTLY BIGGER. IT WAS USED FOR SCREENSHOTS
        CREATION PURPOSES."""

        with open(data_file, "r") as f:
            data = json.load(f)

        all_note_data_sorted = sorted(
            data["sound_occurances_map"].items(),
            key=lambda kv: (kv[1], kv[0]),
            reverse=True,
        )

        names = [n[0] for n in all_note_data_sorted]
        vals = [v[1] for v in all_note_data_sorted]
        x = np.arange(len(names))

        fig, axs = plt.subplots(1, 1)
        axs.bar(x, vals)
        axs.set_xticks(x)
        axs.set_xticklabels(names, fontsize=12)
        axs.set_ylabel("Note occurances", fontsize=20)

        plt.show()

        if is_stegano:
            fig, axs = plt.subplots(1, 1)
            stegano_notes = sorted(
                data["stegano_notes_data"].items(),
                key=lambda kv: (kv[1], kv[0]),
                reverse=True,
            )

            names = [n[0] for n in stegano_notes]
            vals = [v[1] for v in stegano_notes]
            x = np.arange(len(names))
            axs.bar(x, vals)
            axs.set_xticks(x)
            axs.set_xticklabels(names, fontsize=12)
            axs.set_ylabel("Steganographic notes occurances", fontsize=20)

            plt.show()

    def _prepare_data_to_plot(self, data: dict) -> tuple:
        """It prepares the data for plotting. Basically it is sorting unpacking them. It is
        written to be used internally, in other methods. But it is not like you'll go to jail
        for using it in your own scripts.

        Parameters
        ----------
        data : dict
            Dict with data to be sanitized.

        Returns
        -------
        (names, vals, x) : tuple
            It is a tuple useful for plotting, i.e: names are the labels for x-axis, vals are
            values for y-axis and x is just to spread data evenly across the x-axis."""

        sort = sorted(data.items(), key=lambda kv: (kv[1], kv[0]), reverse=True)
        names = [n[0] for n in sort]
        vals = [v[1] for v in sort]
        x = np.arange(len(names))

        return names, vals, x

    def _apply_data_to_plot(
        self, names: list, vals: list, x: list, axs, i: int, label: str
    ):
        """Helper method to apply a bit of DRY into the code. It simply applies the given data
        to a given subplot."""

        axs[i].bar(x, vals)
        axs[i].set_xticks(x)
        axs[i].set_xticklabels(names)
        axs[i].set_ylabel(label)

    def perform_stegano_file_statistical_analysis(self, depth=8):
        """It is very similar to the perform_normal_file_statistical_analysis() method, however
        this also saves/returns the data associated with the steganographic aspects of a file,
        namely: which notes and how many times were they used for steganographic purposes.

        Parameters
        ----------
        depth : int
            It is a hiding depth value, which tells which notes are steganographic ones (i.e.:
            every depth note is used to hide secret message). By default it is set to 8.

        Returns
        -------
        None. It just saves the data to a file with name: "<midi_file_name>.stegano.data"

        Notes
        -----
        Data file is actually a JSON file.

        Also this method utilizes perform_normal_file_statistical_analysis() with save
        parameter set to False in order to get other statistical data. Then on top of that it
        takes care of steganographic aspect of that data."""

        stat_data = self.perform_normal_file_statistical_analysis(save=False)
        out, to_decode, sanitized = self.stegano_music.fetch_secret_message(
            self.midi_file_name, depth
        )
        stegano_notes_data = {}

        for note in to_decode:
            if note not in stegano_notes_data:
                stegano_notes_data[note] = 1
            else:
                stegano_notes_data[note] += 1

        stat_data["stegano_notes_data"] = stegano_notes_data

        data_filename = ".".join([self.midi_file_name.split(".")[0], "stegano", "data"])
        with open(data_filename, "w") as f:
            json.dump(stat_data, f)

    def find_correlation(self):
        """Method which calculates autocorrelation. Surprisingly this was not very useful."""

        midi_vals, list_of_notes, note_names = self.extract_notes(0)
        corr = np.correlate(midi_vals, midi_vals, mode="full")

        return corr[int(corr.size / 2) :]

    def _chunk_generator(self, lst: list, n: int) -> list:
        """Helper method for generating list chunks. Used for musical scale determination."""

        for i in range(0, len(lst), n):
            yield lst[i : i + n]

    def determine_scale(self, use_greek_scales: bool) -> List[tuple]:
        """Method which determines how much a scale fits specific MIDI file.

        Parameters
        ----------
        use_greek_scales : bool
            It tells the musthe module to whether to use greek scales.

        Returns
        -------
        sorted_dict_list : List[tuple]
            List of tuples with pairs ("scale_name", fitness_value)."""

        scales = [s for s in mustheScale.all(use_greek_scales)]
        scale_dict = {key: 0 for key in scales}

        # fetching all sounds from the MIDI file
        sound_names = [
            [p.nameWithOctave for p in n.pitches]
            if isinstance(n, chord.Chord)
            else n.nameWithOctave
            for n in self.get_midi_notes_list()
        ]

        # getting just notes and converting them to musthe notes
        just_notes = [n for n in sound_names if not isinstance(n, list)]
        musthe_notes = [mustheNote(n.replace("-", "b")) for n in just_notes]

        # idea here is to calculate sort-of-probability for each scale. It is not really
        # a probability, but the bigger the value the most fitting is the scale
        all_chunk_number = 0
        for offset in range(1, int(len(musthe_notes) / 2)):
            for note_chunk in self._chunk_generator(musthe_notes, offset):
                scales_to_increment = []
                all_chunk_number += 1
                for scale in scale_dict.keys():
                    if note_chunk in scale:
                        scales_to_increment.append(scale)

                for scale in scales_to_increment:
                    scale_dict[scale] += 1

        # normalising values
        for key in scale_dict.keys():
            scale_dict[key] /= all_chunk_number

        sorted_dict_list = sorted(scale_dict.items(), key=lambda x: x[1])
        return sorted_dict_list

    def extract_notes(self, index: int) -> tuple:
        """It extracts sounds from MIDI file.

        Parameters
        ----------
        index : int
            It represents the track number to extract sounds from

        Returns
        -------
        result : tuple
            Tuple with three lists:
                1. With MIDI values (60 is C4)
                2. List of notes, straight as is
                3. List of human-friendly note names."""

        list_of_notes = []
        midi_value = []
        note_names = []
        for nt in self.base_midi_stream.parts[index].flat.notes:
            if isinstance(nt, note.Note):
                midi_value.append(max(0.0, nt.pitch.ps))
                list_of_notes.append(nt)
                note_names.append(nt.pitch.nameWithOctave)
            elif isinstance(nt, chord.Chord):
                chord_pitches = []
                for pitch in nt.pitches:
                    midi_value.append(max(0.0, pitch.ps))
                    list_of_notes.append(nt)
                    chord_pitches.append(pitch.nameWithOctave)
                note_names.append(chord_pitches)

        return midi_value, list_of_notes, note_names
